import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'dart:convert';

void main() => runApp(Home());

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bluetooth Teste',
      theme: ThemeData(
        primaryColor: Colors.blueAccent,
      ),
      home: BlueHome(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class BlueHome extends StatefulWidget {
  @override
  _BlueHomeState createState() => _BlueHomeState();
}

class _BlueHomeState extends State<BlueHome> {
  FlutterBlue _flutterBlue = FlutterBlue.instance;

  StreamSubscription _scanBlue;
  Map<DeviceIdentifier, ScanResult> scanResults = new Map();
  bool isScanning = false;
  List<Widget> listaTile = [];

  StreamSubscription _stateBlue;
  BluetoothState blueState = BluetoothState.unknown;

  BluetoothDevice device;

  bool get isConnected => (device != null);

  StreamSubscription deviceBlueConnect;
  StreamSubscription deviceStateBlue;

  List<BluetoothService> servicesBlue = List();

  Map<Guid, StreamSubscription> valueChanged = {};

  BluetoothDeviceState deviceState = BluetoothDeviceState.disconnected;

  @override
  void initState() {
    super.initState();

    _flutterBlue.state.then((state) {
      setState(() {
        blueState = state;
        print(blueState.toString());
      });
    });

    _stateBlue = _flutterBlue.onStateChanged().listen((state) {
      setState(() {
        blueState = state;
        print(blueState);
      });
    });
  }

  @override
  void dispose() {
    _stateBlue?.cancel();
    _stateBlue = null;
    _scanBlue?.cancel();
    _scanBlue = null;
    deviceBlueConnect.cancel();
    deviceBlueConnect = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Bluetooth Test",
          style: TextStyle(
            color: Colors.white,
            fontSize: 24.0,
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                children: listaTile ??
                    <Widget>[
                      ListTile(
                        title: Text("Lista Vazia"),
                      )
                    ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: RaisedButton(
                    child: Text("Scan"),
                    onPressed: () {
                      if (isScanning) {
                        _stopScan();
                      } else {
                        _startScan();
                      }
                    },
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: RaisedButton(
                    child: Text("Clean"),
                    onPressed: () {
                      setState(() {
                        listaTile = [];
                      });
                    },
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: RaisedButton(
                    child: Text("Read"),
                    onPressed: () {
                      print(
                          _readCharac(servicesBlue.last.characteristics.first));
                    },
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: RaisedButton(
                    child: Text("Write"),
                    onPressed: () {
                      print(_writeCharac(
                          servicesBlue.last.characteristics.first));
                    },
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: RaisedButton(
                    child: Text("Notify"),
                    onPressed: () {
                      print(_setNotification(
                          servicesBlue.last.characteristics.first));
                    },
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _startScan() {
    _scanBlue = _flutterBlue
        .scan(
      timeout: const Duration(seconds: 5),
    )
        .listen((scanResult) {
      setState(() {
        scanResults[scanResult.device.id] = scanResult;
      });
    }, onDone: _stopScan);
    listaTile = scanResultTiles();
    setState(() {
      isScanning = true;
    });
  }

  _stopScan() {
    _scanBlue?.cancel();
    _scanBlue = null;
    setState(() {
      isScanning = false;
    });
  }

  _connect(BluetoothDevice d) async {
    device = d;

    deviceBlueConnect = _flutterBlue
        .connect(device, timeout: const Duration(seconds: 4))
        .listen(
          null,
          onDone: _disconnect,
        );

    device.state.then((state) {
      setState(() {
        deviceState = state;
      });
    });

    deviceStateBlue = device.onStateChanged().listen((state) {
      setState(() {
        deviceState = state;
      });
      if (state == BluetoothDeviceState.connected) {
        device.discoverServices().then((services) {
          setState(() {
            servicesBlue = services;
          });
        });
      }
    });
  }

  _disconnect() {
    valueChanged.forEach((uuid, sub) => sub.cancel());
    valueChanged.clear();
    deviceStateBlue?.cancel();
    deviceStateBlue = null;
    deviceBlueConnect?.cancel();
    deviceBlueConnect = null;
    setState(() {
      device = null;
    });
  }

  _readCharac(BluetoothCharacteristic c) async {
    await device.readCharacteristic(c);
    setState(() {
      c.value.forEach((f){
        print(f);
      });
    });
  }

  _writeCharac(BluetoothCharacteristic c) async {
    var msg = Utf8Encoder().convert("converte e manda");
    await device.writeCharacteristic(c, msg,
        type: CharacteristicWriteType.withResponse);
    setState(() {});
  }

  _readDescri(BluetoothDescriptor d) async {
    await device.readDescriptor(d);
    setState(() {});
  }

  _writeDescri(BluetoothDescriptor d) async {
    await device.writeDescriptor(d, [0x12, 0x34]);
    setState(() {});
  }

  _setNotification(BluetoothCharacteristic c) async {
    if (c.isNotifying) {
      await device.setNotifyValue(c, false);
      valueChanged[c.uuid]?.cancel();
      valueChanged.remove(c.uuid);
    } else {
      await device.setNotifyValue(c, true);
      final sub = device.onValueChanged(c).listen((d) {
        setState(() {
          var decoded = Utf8Decoder().convert(d); // value == List<int>, Uint8List, etc.
          print(decoded);
          //print('onValueChanged $d');
        });
      });
      valueChanged[c.uuid] = sub;
      
    }
  }

  List<Widget> scanResultTiles() {
    List<Widget> listTiles = [];

    scanResults.forEach((k, v) {
      listTiles.add(
        ListTile(
          contentPadding: EdgeInsets.only(left: 30.0),
          leading: Icon(
            Icons.bluetooth,
            color: Colors.blueAccent,
          ),
          title: v.device.name != ""
              ? Text(
                  "${v.device.name}",
                  style: TextStyle(fontSize: 16.0, color: Colors.blueAccent),
                )
              : Text(
                  "$k",
                  style: TextStyle(fontSize: 16.0, color: Colors.blueAccent),
                ),
          onTap: () {
            if (!isConnected) {
              _connect(v.device);
            }
          },
        ),
      );
      listTiles.add(Divider(
        height: 1.0,
        color: Colors.grey[400],
      ));
    });

    return listTiles;
  }
}
